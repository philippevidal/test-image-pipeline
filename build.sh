#!/bin/bash

set -ev

apk add --no-cache \
    python \
    py-pip \
    ca-certificates \
    openssl \
    groff \
    less \
    bash \
    curl \
    jq \
    git \
    zip \

pip install --no-cache-dir --upgrade pip awscli

aws configure set preview.cloudfront true

export TERRAFORM_VERSION=0.10.8

wget -O terraform.zip https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip
unzip terraform.zip -d /usr/local/bin
rm -f terraform.zip


